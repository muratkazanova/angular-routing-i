import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ColorReferenceItemComponent } from './index';
@NgModule({
  declarations: [ColorReferenceItemComponent],
  imports: [CommonModule],
  exports: [ColorReferenceItemComponent]
})
export class SharedModule {}
