import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {
  EmployeeDashboardComponent,
  EmployeeTaskbarComponent,
  EmployeeFormComponent,
  EmployeeItemComponent,
  EmployeeListComponent,
  EmployeeDetailsComponent
} from './index';
import { SharedModule } from '../shared/shared.module';
import { EmployeeRoutingModule } from './employee-routing.module';

@NgModule({
  declarations: [
    EmployeeDashboardComponent,
    EmployeeTaskbarComponent,
    EmployeeListComponent,
    EmployeeItemComponent,
    EmployeeFormComponent,
    EmployeeDetailsComponent
  ],
  imports: [CommonModule, FormsModule, SharedModule, EmployeeRoutingModule],
  exports: [EmployeeDashboardComponent]
})
export class EmployeeModule {}
