import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeDashboardComponent } from './Containers';
import { EmployeeDetailsComponent } from './Components';
import { EmployeeListResolver } from './Services/employee-list-resolver.service';

const routes: Routes = [
  // {
  //   path: 'employees',
  //   component: EmployeeDashboardComponent,
  //   resolve: { employees: EmployeeListResolver },
  //   runGuardsAndResolvers: 'paramsChange'
  // },
  // { path: 'employees/:id', component: EmployeeDetailsComponent }
  {
    path: '',
    component: EmployeeDashboardComponent,
    resolve: { employees: EmployeeListResolver },
    runGuardsAndResolvers: 'paramsChange'
  },
  { path: ':id', component: EmployeeDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule {}
