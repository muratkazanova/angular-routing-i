import { Component, OnInit } from '@angular/core';
import { IEmployee } from 'src/app/models/employee';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { EmployeeService } from '../../Services/employee.service';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.scss']
})
export class EmployeeDetailsComponent implements OnInit {
  employee: IEmployee;
  id: number;
  constructor(
    private route: ActivatedRoute,
    private employeeService: EmployeeService,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.paramMap
      .pipe(
        switchMap(params => {
          this.id = +params.get('id');
          return this.employeeService.getEmployee(this.id);
        })
      )
      .subscribe((data: IEmployee) => {
        this.employee = data;
      });
  }
  onBack() {
    this.router.navigate(['/employees'], { queryParamsHandling: 'preserve' });
  }
  onNav() {
    this.router.navigate(['/employees', 3], {
      queryParamsHandling: 'preserve'
    });
  }
}
