import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IEmployee } from 'src/app/models/employee';
import { EmployeeService } from '../../Services/employee.service';
import { switchMap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {
  @Input() employees: IEmployee[];
  @Output()
  EmployeeDelete: EventEmitter<number> = new EventEmitter();
  @Output()
  EmployeeUpdate: EventEmitter<IEmployee> = new EventEmitter();
  constructor(
    private employeeService: EmployeeService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {}
}
