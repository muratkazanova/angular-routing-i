import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IEmployee } from 'src/app/models/employee';
import { EmployeeService } from '../../Services/employee.service';

@Component({
  selector: 'app-employee-item',
  templateUrl: './employee-item.component.html',
  styleUrls: ['./employee-item.component.scss']
})
export class EmployeeItemComponent implements OnInit {
  @Input()
  employee: IEmployee;
  isEdit = false;
  detailStyle: string;
  @ViewChild('edtFirstName')
  inputFirstName: ElementRef;
  @ViewChild('edtLastName')
  inputLastName: ElementRef;
  constructor(
    private employeeService: EmployeeService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.queryParamMap.subscribe(params => {
      this.detailStyle = params.has('detailStyle')
        ? params.get('detailStyle')
        : 'inline';
    });
  }

  onEdit(id: number) {
    if (this.detailStyle == 'inline') {
      this.isEdit = true;
    } else {
      this.router.navigate(['/employees', id], {
        queryParamsHandling: 'preserve'
      });
    }
  }
  onEmployeeUpdate(FirstName, LastName, Id) {
    this.employeeService
      .updateEmployee(FirstName, LastName, Id)
      .subscribe(_ => {
        console.log('updated');
        this.isEdit = false;
      });
  }

  onEmployeeDelete(id: number) {
    this.employeeService.deleteEmployee(id).subscribe(_ => {
      // log message
    });
  }

  resetTemplate() {
    this.isEdit = false;
    this.inputFirstName.nativeElement.value = this.employee.FirstName;
    this.inputLastName.nativeElement.value = this.employee.LastName;
  }

  onDetails(id: number) {}
}
