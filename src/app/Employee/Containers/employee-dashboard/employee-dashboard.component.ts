import { Component, OnInit } from '@angular/core';
import { IEmployee } from 'src/app/models/employee';
import { EmployeeService } from '../../Services/employee.service';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
@Component({
  selector: 'app-employee-dashboard',
  templateUrl: './employee-dashboard.component.html',
  styleUrls: ['./employee-dashboard.component.scss']
})
export class EmployeeDashboardComponent implements OnInit {
  employees = [];
  isNewEmployee: boolean;
  constructor(
    private employeeService: EmployeeService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.employeeService.EmployeeFormEvent.subscribe(s => {
      this.isNewEmployee = s;
    });
    this.employeeService.EmployeeSourceChanges.subscribe(_ => {
      this.router.navigate([{ reload: true }], {
        relativeTo: this.route,
        queryParamsHandling: 'preserve'
      });
    });

    this.route.data.subscribe(data => {
      this.employees = data['employees'];
    });
  }
}
