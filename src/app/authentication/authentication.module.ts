import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginFormComponent } from './index';

@NgModule({
  declarations: [LoginFormComponent],
  imports: [CommonModule]
})
export class AuthenticationModule {}
